#ifndef PICTUREITEM_H
#define PICTUREITEM_H

#include <QListWidgetItem>

class PictureItem : public QListWidgetItem
{
public:
    PictureItem();
    PictureItem(const QString & name);
    PictureItem(const QString & name, const QByteArray & pic);

public slots:
    bool setPicture(const QByteArray & pic);
    QByteArray picture();
    void setText(QString str) {
        QListWidgetItem::setText(str.remove("\n"));
    }
    bool isValid();

private:
    QByteArray si_pic;
    bool si_valid;
};

#endif // PICTUREITEM_H
