#include "picture_item.h"

#include <QPainter>

PictureItem::PictureItem():
    QListWidgetItem(0, QListWidgetItem::UserType)
{
    si_valid = false;
}

PictureItem::PictureItem(const QString &name):
    QListWidgetItem(0, QListWidgetItem::UserType)
{
    si_valid = false;
    setText(name);
}

PictureItem::PictureItem(const QString &name, const QByteArray &pic):
    QListWidgetItem(0, QListWidgetItem::UserType)
{
    si_valid = false;
    setText(name);
    setPicture(pic);
}

bool PictureItem::setPicture(const QByteArray &pic)
{
    /*QSvgRenderer svgrenderer(svg.toUtf8());
    if (!svgrenderer.isValid()) {
        return false;
    } else {
        si_valid = true;
    }*/
    // pic поступает к нам в формате base64
    // конвертируем
    QByteArray new_ba = QByteArray::fromBase64(pic);
    QPixmap pixmap(64, 64);
    bool res = pixmap.loadFromData(new_ba);
    if (!res){
        return false;
    } else {
        si_valid = true;
    }
    setIcon(QIcon(pixmap));
    /*QPainter painter(&pixmap);
    painter.save();
    painter.setPen(Qt::NoPen);
    painter.setBrush(QColor(Qt::white));
    painter.drawRect(QRect(0, 0, 64, 64));
    painter.drawPixmap(QRect(0, 0, 64, 64),pixmap,QRect(0, 0, 64, 64));
    painter.restore();
    //svgrenderer.render(&painter);
    setIcon(QIcon(pixmap));*/
    si_pic = new_ba;
    return true;
}

QByteArray PictureItem::picture()
{
    QByteArray base64 = si_pic.toBase64();
    return base64;
}

bool PictureItem::isValid()
{
    return si_valid;
}
